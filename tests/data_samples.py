from datetime import datetime

user1 = {
    "name": "User1 Name",
    "login": "user1_login",
    "password": "user1_password",
}

user2 = {
    "name": "User2 Name",
    "login": "user2_login",
    "password": "user2_password",
}

# user1_receipt = {
#    "products": [{"name": "Product One", "price_per_unit": 10, "quantity": 5}],
#    "payment": {"type": "cash", "amount": 100},
# }
user1_receipt = {
    "products": [
        {"name": "Product One", "price_per_unit": 2.5, "quantity": 6},
        {"name": "Product Two", "price_per_unit": 10.99, "quantity": 2.4},
        {"name": "Product Three", "price_per_unit": 1999.99, "quantity": 1},
        {"name": "Product Four", "price_per_unit": 6, "quantity": 2.6},
        {
            "name": "Product With Super Mega Ultra Very Long Name",
            "price_per_unit": 10,
            "quantity": 5,
        },
    ],
    "payment": {"type": "cash", "amount": 4000},
}


def get_receipt_text(receipt: dict[str]):
    date_time = datetime.strptime(receipt["created_at"], "%Y-%m-%dT%H:%M:%S.%f")
    return (
        "           User1 Name           \n"
        "================================\n"
        "6.00 x 2.50                     \n"
        "Product One                15.00\n"
        "--------------------------------\n"
        "2.40 x 10.99                    \n"
        "Product Two                26.38\n"
        "--------------------------------\n"
        "1.00 x 1999.99                  \n"
        "Product Three            1999.99\n"
        "--------------------------------\n"
        "2.60 x 6.00                     \n"
        "Product Four               15.60\n"
        "--------------------------------\n"
        "5.00 x 10.00                    \n"
        "Product With Super Mega         \n"
        "Ultra Very Long Name       50.00\n"
        "================================\n"
        "СУМА                     2106.97\n"
        "Готівка                  4000.00\n"
        "Решта                    1893.03\n"
        "================================\n"
        f"        {date_time:%d.%m.%Y %H:%M}        \n"
        "      Дякуємо за покупку!       "
    )


user2_receipt_list = [
    {
        "products": [{"name": "Product 1", "price_per_unit": 10, "quantity": 1}],
        "payment": {"type": "cashless", "amount": 10},
    },
    {
        "products": [{"name": "Product 2", "price_per_unit": 20, "quantity": 2}],
        "payment": {"type": "cashless", "amount": 40},
    },
    {
        "products": [{"name": "Product 3", "price_per_unit": 30, "quantity": 3}],
        "payment": {"type": "cash", "amount": 100},
    },
    {
        "products": [{"name": "Product 4", "price_per_unit": 40, "quantity": 4}],
        "payment": {"type": "cashless", "amount": 160},
    },
    {
        "products": [{"name": "Product 4", "price_per_unit": 50, "quantity": 5}],
        "payment": {"type": "cash", "amount": 250},
    },
    {
        "products": [{"name": "Product 4", "price_per_unit": 60, "quantity": 6}],
        "payment": {"type": "cash", "amount": 400},
    },
]
