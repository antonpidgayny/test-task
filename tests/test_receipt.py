from .conftest import TestClient
from .data_samples import (
    user1,
    user2,
    user1_receipt,
    user2_receipt_list,
    get_receipt_text,
)


def create_receipt(app: TestClient, data: dict[str]) -> dict:
    resp = app.post("/receipt/", json=data)
    assert resp.status_code == 200, f"Detail: {resp.text}"
    data = resp.json()
    return data


def test_create(test_app_authed):
    app: TestClient = next(test_app_authed(user1["access_token"]))
    data = create_receipt(app, user1_receipt)
    assert "id" in data
    assert "created_at" in data

    for p in user1_receipt["products"]:
        p["total"] = p["quantity"] * p["price_per_unit"]

    assert user1_receipt["products"] == data["products"]
    assert user1_receipt["payment"] == data["payment"]

    total = sum(p["total"] for p in user1_receipt["products"])
    assert total == data["total"]
    assert (user1_receipt["payment"]["amount"] - total) == data["rest"]
    user1_receipt.update(data)


def test_get_one(test_app_authed):
    app: TestClient = next(test_app_authed(user1["access_token"]))
    resp = app.get(f"/receipt/{user1_receipt['id']}")
    data = resp.json()
    assert user1_receipt == data


def compare_reciept_all(li1: list[dict[str]], li2: list[dict[str]]):
    for el1, el2 in zip(li1, li2):
        assert el1["id"] == el2["id"]
        assert el1["total"] == el2["total"]
        assert el1["rest"] == el2["rest"]
        assert el1["created_at"] == el2["created_at"]

        assert el1["payment"]["amount"] == el2["payment_amount"]
        assert el1["payment"]["type"] == el2["payment_type"]


def test_get_all(test_app_authed):
    app: TestClient = next(test_app_authed(user2["access_token"]))
    for receipt in user2_receipt_list:
        receipt.update(create_receipt(app, receipt))
    resp = app.get(f"/receipt/?pageSize={len(user2_receipt_list)}")
    compare_reciept_all(user2_receipt_list, resp.json())


def test_pagination(test_app_authed):
    app: TestClient = next(test_app_authed(user2["access_token"]))
    page_size = 5
    page_number = 1
    resp = app.get(f"/receipt/?pageSize={page_size}&pageNumber={page_number}")
    data = resp.json()
    compare_reciept_all(user2_receipt_list[:page_size], data)
    page_number = 2
    resp = app.get(f"/receipt/?pageSize={page_size}&pageNumber={page_number}")
    data = resp.json()
    compare_reciept_all(user2_receipt_list[page_size : page_size * page_number], data)


def test_filtration(test_app_authed):
    app: TestClient = next(test_app_authed(user2["access_token"]))
    cash_only_list = list(
        filter(lambda x: x["payment"]["type"] == "cash", user2_receipt_list)
    )
    resp = app.get(f"/receipt/?pageSize={len(user2_receipt_list)}&paymentType=cash")
    compare_reciept_all(cash_only_list, resp.json())
    cashless_only_list = list(
        filter(lambda x: x["payment"]["type"] == "cashless", user2_receipt_list)
    )
    resp = app.get(f"/receipt/?pageSize={len(user2_receipt_list)}&paymentType=cashless")
    compare_reciept_all(cashless_only_list, resp.json())
    total_min = 90
    total_max = 250
    betwen_min_max = list(
        filter(lambda x: total_min <= x["total"] <= total_max, user2_receipt_list)
    )
    resp = app.get(
        f"/receipt/?pageSize={len(user2_receipt_list)}&totalMin={total_min}&totalMax={total_max}"
    )
    compare_reciept_all(betwen_min_max, resp.json())


def test_public_text_view(test_app: TestClient):
    resp = test_app.get(f"/receipt/{user1_receipt['id']}/text")
    assert resp.text == get_receipt_text(user1_receipt)


def test_not_found(test_app_authed):
    app: TestClient = next(test_app_authed(user1["access_token"]))
    wrong_id = "97a44ab1-f8ac-474b-a770-d59afd496ca1"
    resp = app.get(f"/receipt/{wrong_id}")
    assert resp.status_code == 404
    assert resp.json()["detail"] == f"Reciept with id {wrong_id} not found."
