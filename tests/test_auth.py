from .conftest import TestClient
from .data_samples import (
    user1,
    user2,
)


def user_signup(app: TestClient, user: dict[str, str]) -> dict[str]:
    resp = app.post("/auth/signup", json=user)
    assert resp.status_code == 200, f"Detail: {resp.text}"
    data = resp.json()
    assert "id" in data
    assert "created_at" in data
    assert data["name"] == user["name"]
    assert data["login"] == user["login"]
    return data


def test_signup(test_app: TestClient):
    user1.update(user_signup(test_app, user1))
    user2.update(user_signup(test_app, user2))


def user_signin(app: TestClient, user: dict[str]) -> str:
    data = {"login": user["login"], "password": user["password"]}
    resp = app.post("/auth/signin", json=data)
    assert resp.status_code == 200, f"Detail: {resp.text}"
    data = resp.json()

    assert "token" in data["access_token"]
    assert data["access_token"]["type"] == "Bearer"

    assert "token" in data["refresh_token"]
    assert data["refresh_token"]["type"] == "Bearer"

    return {
        "access_token": data["access_token"]["token"],
        "refresh_token": data["refresh_token"]["token"],
    }


def test_signin(test_app: TestClient) -> str:
    user1.update(user_signin(test_app, user1))
    user2.update(user_signin(test_app, user2))


def user_me(app: TestClient, user: dict[str]) -> str:
    resp = app.get(
        "/auth/me", headers={"Authorization": f'Bearer {user["access_token"]}'}
    )
    assert resp.status_code == 200, f"Detail: {resp.text}"
    data = resp.json()
    assert data["id"] == user["id"]
    assert data["name"] == user["name"]
    assert data["login"] == user["login"]
    assert data["created_at"] == user["created_at"]


def test_me(test_app: TestClient) -> str:
    user_me(test_app, user1)
    user_me(test_app, user2)


def test_invalid_login_password(test_app: TestClient):
    data = {"login": "wrong_login", "password": user1["password"]}
    resp = test_app.post("/auth/signin", json=data)
    assert resp.status_code == 401, f"Detail: {resp.text}"
    assert resp.json()["detail"] == "Incorect login or password"

    data = {"login": user1["login"], "password": "wrong_password"}
    resp = test_app.post("/auth/signin", json=data)
    assert resp.status_code == 401, f"Detail: {resp.text}"
    assert resp.json()["detail"] == "Incorect login or password"


def test_auth_required(test_app: TestClient) -> str:
    resp = test_app.get("/auth/me")
    assert resp.status_code == 403, f"Detail: {resp.text}"
    data = resp.json()
    assert data["detail"] == "Not authenticated"


def test_invalid_token(test_app: TestClient) -> str:
    resp = test_app.get("/auth/me", headers={"Authorization": "Bearer dwdawdawd"})
    assert resp.status_code == 401, f"Detail: {resp.text}"
    data = resp.json()
    assert data["detail"] == "Invalid token."
