import pytest
from httpx import Auth, Request
from fastapi.testclient import TestClient

from src.main import app


class CustomAuth(Auth):
    def __init__(self, user: dict[str]):
        self.access_token = user.get("access_token", None)
        self.refresh_token = user.get("refresh_token", None)
        self.login = user.get("login")
        self.password = user.get("password")
        self.refresh_url = "/auth/refresh"
        self.signin_url = "/auth/signin"

    def auth_flow(self, request):
        if self.access_token is None or self.refresh_token is None:
            signin_response = yield self.signin_request()
            self.update_tokens(signin_response)

        request.headers["X-Authentication"] = f"Bearer {self.access_token}"
        response = yield request

        if response.status_code == 401 and "Token is expired." in response.text:
            refresh_response = yield self.refresh_request()
            self.refresh_token(refresh_response)
            request.headers["Authorization"] = f"Bearer {self.access_token}"
            yield request

    def signin_request(self) -> Request:
        return Request(
            "post",
            self.signin_url,
            json={
                "login": self.login,
                "password": self.password,
            },
        )

    def refresh_request(self) -> Request:
        return Request(
            "post", self.refresh_url, headers={"Authorization": self.refresh_token}
        )

    def refresh_token(self, response) -> Request:
        data = response.json()
        self.access_token = data["token"]

    def update_tokens(self, response):
        data = response.json()
        self.access_token = data["access_token"]["token"]
        self.refresh_token = data["refresh_token"]["token"]


@pytest.fixture(scope="module")
def test_app() -> TestClient:
    with TestClient(app) as test_client:
        yield test_client


@pytest.fixture(scope="module")
def test_app_authed() -> TestClient:
    def get_app(token: str):
        with TestClient(
            app, headers={"Authorization": f"Bearer {token}"}
        ) as test_client:
            yield test_client

    return get_app
