from fastapi.exceptions import HTTPException
from fastapi import status


class UserAlreadyExists(HTTPException):
    def __init__(self, login: str):
        msg = f"User with login {login} already exists."
        super().__init__(status_code=status.HTTP_409_CONFLICT, detail=msg)


class UserNotFound(HTTPException):
    def __init__(self):
        msg = "User not found."
        super().__init__(status_code=status.HTTP_404_NOT_FOUND, detail=msg)


class IncorectLoginOrPassword(HTTPException):
    def __init__(self):
        msg = "Incorect login or password"
        super().__init__(status_code=status.HTTP_401_UNAUTHORIZED, detail=msg)


class InvalidToken(HTTPException):
    def __init__(self):
        msg = "Invalid token."
        super().__init__(status_code=status.HTTP_401_UNAUTHORIZED, detail=msg)


class AuthReqiured(HTTPException):
    def __init__(self):
        msg = "Auth required."
        super().__init__(status_code=status.HTTP_401_UNAUTHORIZED, detail=msg)


class TokenExpired(HTTPException):
    def __init__(self):
        msg = "Token is expired."
        super().__init__(status_code=status.HTTP_401_UNAUTHORIZED, detail=msg)


class InvalidScope(HTTPException):
    def __init__(self):
        msg = "Scope for the token is invalid."
        super().__init__(status_code=status.HTTP_401_UNAUTHORIZED, detail=msg)
