from typing import Annotated
from fastapi import Depends
from sqlalchemy import Select
from sqlalchemy.exc import IntegrityError, NoResultFound

from ..dependencies import db_dependency
from .schema import UserPost
from .models import User
from .utils import hash_password


class UserCrud:
    def __init__(self, session: db_dependency):
        self.session = session

    def create_user(self, data: UserPost) -> User | None:
        hashed_password = hash_password(data.password)
        user = User(hashed_password=hashed_password, **data.dict(exclude={"password"}))
        self.session.add(user)
        try:
            self.session.commit()
        except IntegrityError:
            self.session.rollback()
            return None
        self.session.refresh(user)
        return user

    def get_user_by_id(self, id: int) -> User | None:
        try:
            user = self.session.get_one(User, id)
        except NoResultFound:
            user = None
        return user

    def get_user_by_login(self, login: str) -> User | None:
        qry = Select(User).filter(User.login == login)
        user: User = self.session.execute(qry).scalar_one_or_none()
        return user


user_crud_dependency = Annotated[UserCrud, Depends(UserCrud)]
