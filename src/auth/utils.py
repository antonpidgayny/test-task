from datetime import timedelta, datetime
from enum import StrEnum
from passlib.context import CryptContext
from jose import jwt, ExpiredSignatureError, JWTError

from .exceptions import TokenExpired, InvalidToken, InvalidScope
from .config import (
    PASS_HASH_SALT,
    ACCESS_TOKEN_EXPIRES_MINUTES,
    REFRESH_TOKEN_EXPIRES_DAYS,
    SECRET_KEY,
    TOKEN_ALGORITHM,
)


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


class TokenType(StrEnum):
    access = "access_token"
    refresh = "refresh_token"


def hash_password(password: str) -> str:
    return pwd_context.hash(f"{password}{PASS_HASH_SALT}")


def verify_password(plain_password: str, hashed_password: str) -> bool:
    return pwd_context.verify(f"{plain_password}{PASS_HASH_SALT}", hashed_password)


def encode_token(login: str, expires: timedelta, token_type: TokenType) -> str:
    payload = {
        "exp": datetime.utcnow() + expires,
        "iat": datetime.utcnow(),
        "scope": token_type,
        "sub": login,
    }
    return jwt.encode(payload, SECRET_KEY, algorithm=TOKEN_ALGORITHM)


def encode_access_token(login: str) -> str:
    return encode_token(
        login,
        expires=timedelta(minutes=ACCESS_TOKEN_EXPIRES_MINUTES),
        token_type=TokenType.access,
    )


def encode_refresh_token(login: str) -> str:
    return encode_token(
        login,
        expires=timedelta(days=REFRESH_TOKEN_EXPIRES_DAYS),
        token_type=TokenType.refresh,
    )


def decode_token(token: str, token_type: TokenType) -> dict:
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[TOKEN_ALGORITHM])
        if payload["scope"] != token_type:
            raise InvalidScope
    except ExpiredSignatureError:
        raise TokenExpired
    except JWTError:
        raise InvalidToken
    return payload


def decode_refresh_token(token: str) -> dict:
    return decode_token(token, token_type=TokenType.refresh)


def decode_access_token(token: str) -> dict:
    return decode_token(token, token_type=TokenType.access)
