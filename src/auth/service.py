from fastapi import Depends
from typing import Annotated

from .crud import user_crud_dependency, UserCrud
from .schema import UserPost, UserGet, Token, Tokens
from .exceptions import (
    UserAlreadyExists,
    IncorectLoginOrPassword,
    UserNotFound,
)
from .utils import (
    verify_password,
    encode_access_token,
    encode_refresh_token,
    decode_refresh_token,
)


class UserService:
    def __init__(self, crud: user_crud_dependency):
        self.crud: UserCrud = crud

    def create_user(self, data: UserPost) -> UserGet:
        user = self.crud.create_user(data)
        if user is None:
            raise UserAlreadyExists(data.login)
        return UserGet.from_orm(user)

    def get_user_by_id(self, id: int) -> UserGet:
        user = self.crud.get_user_by_id(id)
        if user is None:
            raise UserNotFound
        return UserGet.from_orm(user)

    def get_user_by_login(self, login: str) -> UserGet:
        user = self.crud.get_user_by_login(login)
        if user is None:
            raise UserNotFound
        return UserGet.from_orm(user)


user_service_dependency = Annotated[UserService, Depends(UserService)]


class TokenService:
    def __init__(self, crud: user_crud_dependency):
        self.crud: UserCrud = crud

    def _authenticate_user(self, password: str, login: str) -> UserGet:
        user = self.crud.get_user_by_login(login)
        if user is None:
            raise IncorectLoginOrPassword
        if not verify_password(password, user.hashed_password):
            raise IncorectLoginOrPassword
        return UserGet.from_orm(user)

    def create_access_token(self, login: str) -> Token:
        token = encode_access_token(login)
        return Token(token=token)

    def create_refresh_token(self, login: str) -> Token:
        token = encode_refresh_token(login)
        return Token(token=token)

    def signin(self, password: str, login: str) -> Tokens:
        user = self._authenticate_user(password, login)
        tokens = Tokens(
            access_token=self.create_access_token(user.login),
            refresh_token=self.create_refresh_token(user.login),
        )
        return tokens

    def refresh_access_token(self, token: str) -> str:
        data = decode_refresh_token(token)
        return self.create_access_token(data["sub"])

    def revoke_refresh_token(self): ...


token_service_dependency = Annotated[TokenService, Depends(TokenService)]
