from typing import Annotated
from fastapi import Depends
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer

from .utils import decode_access_token
from .exceptions import (
    UserNotFound,
)
from .crud import user_crud_dependency, User


access_token_security = HTTPBearer(scheme_name="Access Token")
refresh_token_security = HTTPBearer(scheme_name="Refresh Token")

access_token_dependency = Annotated[
    HTTPAuthorizationCredentials | None, Depends(access_token_security)
]
refresh_token_dependency = Annotated[
    HTTPAuthorizationCredentials | None, Depends(refresh_token_security)
]


def get_current_user(
    crud: user_crud_dependency,
    creds: access_token_dependency,
) -> User:
    token: str = creds.credentials
    payload = decode_access_token(token)
    login = payload["sub"]

    user = crud.get_user_by_login(login)
    if user is None:
        raise UserNotFound
    return user


current_user_dependency = Annotated[User, Depends(get_current_user)]
