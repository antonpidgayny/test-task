from fastapi import APIRouter

from .service import user_service_dependency, token_service_dependency
from .dependencies import (
    current_user_dependency,
    refresh_token_dependency,
)
from .schema import UserGet, UserPost, Token, Tokens, LoginPassword


router = APIRouter(prefix="/auth", tags=["auth"])


@router.post("/signup", response_model=UserGet)
def create_new_user(data: UserPost, service: user_service_dependency) -> UserGet:
    return service.create_user(data)


@router.post("/signin", response_model=Tokens)
def get_tokens(service: token_service_dependency, data: LoginPassword) -> Token:
    return service.signin(login=data.login, password=data.password)


@router.post("/refresh", response_model=Token)
def create_new_access_token(
    service: token_service_dependency, creds: refresh_token_dependency
) -> Token:
    return service.refresh_access_token(creds.credentials)


@router.get("/me", response_model=UserGet)
def get_current_user(user_db: current_user_dependency) -> UserGet:
    user = UserGet.from_orm(user_db)
    return user
