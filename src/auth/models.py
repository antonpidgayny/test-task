from typing import Annotated
from datetime import datetime

from sqlalchemy import (
    func,
    String,
)
from sqlalchemy.orm import Mapped, mapped_column, relationship

from ..db import Base


int_pk = Annotated[int, mapped_column(primary_key=True)]
created_at = Annotated[datetime, mapped_column(server_default=func.now())]
str_128 = Annotated[str, mapped_column(String(128))]


class User(Base):
    __tablename__ = "users"

    id: Mapped[int_pk]
    name: Mapped[str_128]
    login: Mapped[str] = mapped_column(String(128), unique=True)
    hashed_password: Mapped[str] = mapped_column(String(128), deferred=True)
    created_at: Mapped[created_at]

    reciepts: Mapped[list["Reciept"]] = relationship(back_populates="user")  # noqa: F821
