from datetime import datetime

from ..schema import CustomBase, str_field_128


class LoginPassword(CustomBase):
    login: str_field_128
    password: str_field_128


class UserPost(LoginPassword):
    name: str_field_128
    login: str_field_128
    password: str_field_128


class UserGet(CustomBase):
    id: int
    name: str_field_128
    login: str_field_128
    created_at: datetime


class Token(CustomBase):
    token: str
    type: str = "Bearer"


class Tokens(CustomBase):
    access_token: Token
    refresh_token: Token
