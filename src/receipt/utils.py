from .config import (
    DEFAULT_LINE_LENGTH,
    DEFAULT_SECTION_SEP,
    DEFAULT_PRODUCTS_SEP,
    DEFAULT_FOOTER_TEXT,
)
from .models import Reciept, Product, PaymentType


payment_type = {
    PaymentType.cash: "Готівка",
    PaymentType.cashless: "Картка",
}


class RecieptFormater:
    def __init__(
        self,
        receipt: Reciept,
        line_length: int = DEFAULT_LINE_LENGTH,
        section_sep: str = DEFAULT_SECTION_SEP,
        products_sep: str = DEFAULT_PRODUCTS_SEP,
        footer_text: str = DEFAULT_FOOTER_TEXT,
    ):
        self.receipt = receipt
        self.line_length = line_length
        self.section_sep = section_sep
        self.products_sep = products_sep
        self.footer_text = footer_text
        self.section_sep = self.section_sep * self.line_length + "\n"
        self.products_sep = self.products_sep * self.line_length + "\n"

    def _split_string_if_too_long(self, string: str, max_length: int) -> list[str]:
        if len(string) <= max_length:
            return [string]
        chunks = []
        current_chunk = ""
        for word in string.split():
            if len(word) > max_length:
                chunks.extend(
                    [word[i : i + max_length] for i in range(0, len(word), max_length)]
                )
            elif len(current_chunk) + len(word) <= max_length:
                current_chunk += word + " "
            else:
                chunks.append(current_chunk.strip())
                current_chunk = word + " "
        if current_chunk:
            chunks.append(current_chunk.strip())

        return chunks

    def _two_strings_one_line(self, left_str: str, right_str: str) -> str:
        right_len = len(right_str)
        left_str_li = self._split_string_if_too_long(
            left_str, self.line_length - (right_len + 1)
        )
        left_str = "\n".join(f"{s:<{self.line_length}}" for s in left_str_li[:-1])
        left_str += "\n" if left_str else ""
        last_left_str = left_str_li[-1]
        space_len = self.line_length - (len(last_left_str) + right_len)
        space_len = space_len if space_len > 0 else 1
        return f"{left_str}{last_left_str}{' '*space_len}{right_str}"

    def _get_product_string(self, product: Product) -> str:
        first_line = f"{f'{product.quantity:.2f} x {product.price_per_unit:.2f}':<{self.line_length}}\n"
        second_line = self._two_strings_one_line(product.name, f"{product.total:.2f}")
        return f"{first_line}{second_line}\n"

    def _get_header(self) -> str:
        return f"{self.receipt.user.name:^{self.line_length}}\n"

    def _get_products(self) -> str:
        products_strings = []
        for product in self.receipt.products:
            p_string = self._get_product_string(product)
            products_strings.append(p_string)
        products_string = self.products_sep.join(products_strings)
        return products_string

    def _get_total(self) -> str:
        total_sum_string = self._two_strings_one_line(
            "СУМА", f"{self.receipt.total:.2f}"
        )
        total_payment_string = self._two_strings_one_line(
            payment_type[self.receipt.payment.type],
            f"{self.receipt.payment.amount:.2f}",
        )
        total_rest_string = self._two_strings_one_line(
            "Решта", f"{self.receipt.rest:.2f}"
        )
        total = f"{total_sum_string}\n{total_payment_string}\n{total_rest_string}\n"
        return total

    def _get_footer(self) -> str:
        created_at = (
            f"{f'{self.receipt.created_at:%d.%m.%Y %H:%M}':^{self.line_length}}"
        )
        footer_text_formated = f"{self.footer_text:^{self.line_length}}"
        footer = f"{created_at}\n{footer_text_formated}"
        return footer

    def get_reciept_text_visual(self) -> str:
        header_string = self._get_header()
        products_string = self._get_products()
        total_string = self._get_total()
        footer_string = self._get_footer()

        receipt_string = (
            f"{header_string}{self.section_sep}"
            f"{products_string}{self.section_sep}"
            f"{total_string}{self.section_sep}"
            f"{footer_string}"
        )
        return receipt_string
