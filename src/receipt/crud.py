import uuid
from fastapi import Depends
from typing import Annotated, Iterable
from sqlalchemy import (
    select,
)
from sqlalchemy.exc import NoResultFound

from ..dependencies import (
    Session,
    db_dependency,
)
from ..auth.models import User
from .models import (
    Reciept,
    Payment,
    Product,
)
from .schema import (
    RecieptPost,
)
from .filters import RecieptFilter
from .pagination import Paginator


class RecieptCrud:
    def __init__(self, session: db_dependency):
        self.session: Session = session

    def get_all_receipts(
        self, filter: RecieptFilter, user: User, paginator: Paginator
    ) -> Iterable[Reciept]:
        qry = (
            select(Reciept)
            .join(Reciept.payment)
            .join(Reciept.products)
            .filter(Reciept.user_id == user.id)
        )
        qry = filter.filter(qry)
        qry = filter.sort(qry)
        qry = paginator.paginate(qry)
        res = self.session.execute(qry).unique().scalars()
        return res

    def get_receipt_with_id(self, id: uuid.UUID, user: User) -> Reciept | None:
        try:
            reciept = self.session.get_one(Reciept, id)
        except NoResultFound:
            return None
        return reciept if reciept.user_id == user.id else None

    def get_receipt_with_id_no_auth(self, id: uuid.UUID) -> Reciept | None:
        try:
            reciept = self.session.get_one(Reciept, id)
        except NoResultFound:
            reciept = None
        return reciept

    def create_receipt(self, data: RecieptPost, user: User) -> Reciept:
        payment = Payment(**data.payment.dict())
        products = [Product(**p.dict()) for p in data.products]
        reciept = Reciept(
            user=user,
            payment=payment,
            products=products,
        )
        self.session.add(reciept)
        self.session.commit()
        self.session.refresh(reciept)
        return reciept


crud_dependency = Annotated[RecieptCrud, Depends(RecieptCrud)]
