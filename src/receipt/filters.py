from typing import Annotated
from datetime import datetime

from fastapi_filter import FilterDepends, with_prefix
from fastapi_filter.contrib.sqlalchemy import Filter
from pydantic import (
    Field,
    model_validator,
    field_validator,
)

from .models import PaymentType, Reciept, Payment


class PaymentFilter(Filter):
    type: PaymentType | None = Field(
        default=None,
        alias="paymentType",
        name="Тип оплати",
        description='Тип оплати: "cash" або "cashless"',
    )

    class Constants(Filter.Constants):
        model = Payment


class RecieptFilter(Filter):
    total__gte: float | None = Field(
        default=None,
        ge=0,
        alias="totalMin",
        name="Мінімальна сума",
        description="Мінімальна вартість усіх товарів у чеку",
    )
    total__lte: float | None = Field(
        default=None,
        ge=0,
        alias="totalMax",
        name="Максимальна сума",
        description="Максимальна вартість усіх товарів у чеку",
    )
    created_at__gte: datetime | None = Field(
        default=None,
        alias="createdAtStart",
        name="Мінімальна дата-час",
        description="Мінімальна дата створення чеку",
    )
    created_at__lte: datetime | None = Field(
        default=None,
        alias="createdAtEnd",
        name="Максимальна дата-час",
        description="Максимальна дата створення чеку",
    )
    payment: PaymentFilter | None = FilterDepends(
        with_prefix("payment", PaymentFilter), by_alias=True
    )
    order_by: list[str] | None = Field(
        default=None,
        alias="orderBy",
        name="Сортування",
        description='Список полів для сортування. Можливі значення: "+total", "-total", "+created_at", "-created_at"',
    )

    @model_validator(mode="after")
    def validate(self):
        if self.total__gte is not None and self.total__lte is not None:
            if self.total__lte < self.total__gte:
                raise ValueError("totalMax must be greater or equal totalMin")
        if self.created_at__gte is not None and self.created_at__lte is not None:
            if self.created_at__lte < self.created_at__gte:
                raise ValueError("createdAtEnd must be greater or equal createdAtStart")

    @field_validator("order_by")
    def restrict_sortable_fields(cls, value):
        if value is None:
            return None
        allowed_field_names = ["total", "created_at"]
        for field_name in value:
            field_name = field_name.replace("+", "").replace("-", "")
            if field_name not in allowed_field_names:
                raise ValueError(
                    f"You may only sort by: {', '.join(allowed_field_names)}"
                )
        return value

    class Constants(Filter.Constants):
        model = Reciept


reciept_filter_dependency = Annotated[
    RecieptFilter, FilterDepends(RecieptFilter, by_alias=True)
]
