import uuid
from fastapi import HTTPException, status


class RecieptNotFound(HTTPException):
    def __init__(self, reciept_id: uuid.UUID):
        msg = f'Reciept with id {reciept_id} not found.'
        super().__init__(status_code=status.HTTP_404_NOT_FOUND, detail=msg)