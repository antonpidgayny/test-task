import uuid

from typing import Annotated
from datetime import datetime
from enum import StrEnum, auto

from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    column_property,
)
from sqlalchemy import (
    select,
    String,
    ForeignKey,
    UUID,
    func,
    text,
    Computed,
)

from ..db import Base
from ..auth.models import User


int_pk = Annotated[int, mapped_column(primary_key=True)]
uuid_pk = Annotated[uuid.UUID, mapped_column(
    UUID(as_uuid=True),
    primary_key=True,
    server_default=text("gen_random_uuid()")
)]
created_at = Annotated[datetime, mapped_column(server_default=func.now())]
str_128 = Annotated[str, mapped_column(String(128))]
reciept_fk = Annotated[uuid.UUID, mapped_column(
    ForeignKey('reciepts.id'),
    index=True
)]
payment_fk = Annotated[int, mapped_column(ForeignKey('payments.id'))]


class PaymentType(StrEnum):
    cash: str = auto()
    cashless: str = auto()


class Product(Base):
    __tablename__ = 'products'

    id: Mapped[int_pk]
    name: Mapped[str_128]
    price_per_unit: Mapped[float] = mapped_column()
    quantity: Mapped[float] = mapped_column()

    total: Mapped[float] = mapped_column(Computed(price_per_unit * quantity))

    reciept_id: Mapped[reciept_fk]
    reciept: Mapped["Reciept"] = relationship(back_populates='products')


class Payment(Base):
    __tablename__ = 'payments'

    id: Mapped[int_pk]
    type: Mapped[PaymentType]
    amount: Mapped[float]

    reciept_id: Mapped[reciept_fk]
    reciept: Mapped["Reciept"] = relationship(
        back_populates='payment',
        single_parent=True
    )


class Reciept(Base):
    __tablename__ = 'reciepts'

    id: Mapped[uuid.UUID] = mapped_column(
        UUID(as_uuid=True),
        primary_key=True,
        server_default=text("gen_random_uuid()")
    )
    created_at: Mapped[created_at]
    user_id: Mapped[int] = mapped_column(ForeignKey('users.id'))

    user: Mapped['User'] = relationship(back_populates='reciepts')
    payment: Mapped[Payment] = relationship(back_populates='reciept')
    products: Mapped[list[Product]] = relationship(back_populates='reciept')

    total: Mapped[float] = column_property(
        select(func.sum(Product.total))
        .where(Product.reciept_id == id)
        .correlate_except(Product)
        .scalar_subquery()
    )

    rest: Mapped[float] = column_property(
        select(Payment.amount)
        .where(Payment.reciept_id == id)
        .correlate_except(Payment)
        .scalar_subquery() - total
    )
