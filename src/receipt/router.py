import uuid
import qrcode
import io
from fastapi import (
    APIRouter,
    Query,
)
from fastapi.responses import PlainTextResponse, StreamingResponse

from .schema import (
    RecieptGet,
    RecieptPost,
    RecieptGetAll,
    Email,
    Messenger,
)
from .config import DEFAULT_LINE_LENGTH
from ..config import DOMAIN
from ..auth.dependencies import current_user_dependency
from .service import service_dependency
from .filters import reciept_filter_dependency
from .pagination import paginator_dependency


router = APIRouter(prefix="/receipt", tags=["receipt"])


@router.post("/", response_model=RecieptGet)
async def create_receipt(
    data: RecieptPost, service: service_dependency, user: current_user_dependency
) -> RecieptGet:
    return service.create_receipt(data, user)


@router.get("/", response_model=list[RecieptGetAll])
async def get_all_receipts(
    service: service_dependency,
    filter: reciept_filter_dependency,
    user: current_user_dependency,
    paginator: paginator_dependency,
) -> list[RecieptGetAll]:
    return service.get_all_receipts(filter, user, paginator)


@router.get("/{id}", response_model=RecieptGet)
async def get_receipt(
    id: uuid.UUID, service: service_dependency, user: current_user_dependency
) -> RecieptGet:
    return service.get_receipt_with_id(id, user)


@router.get("/{id}/text", response_class=PlainTextResponse)
async def get_receipt_text(
    id: uuid.UUID,
    service: service_dependency,
    line_lenght: int | None = Query(
        default=DEFAULT_LINE_LENGTH, ge=16, le=64, alias="lineLenght"
    ),
) -> str:
    return service.get_receipt_text(id, line_length=line_lenght)


@router.get("/{id}/qr", response_class=StreamingResponse)
async def get_receipt_qr(
    id: uuid.UUID,
    service: service_dependency,
    line_lenght: int | None = Query(
        default=DEFAULT_LINE_LENGTH, ge=16, le=64, alias="lineLenght"
    ),
) -> StreamingResponse:
    img = qrcode.make(f"{DOMAIN}/receipt/{id}/text?lineLenght={line_lenght}")
    buf = io.BytesIO()
    img.save(buf)
    buf.seek(0)
    return StreamingResponse(buf, media_type="image/jpeg")


@router.post("/{id}/send-to-email")
async def send_receipt_to_email(
    id: uuid.UUID,
    service: service_dependency,
    data: Email,
    line_lenght: int | None = Query(
        default=DEFAULT_LINE_LENGTH, ge=16, le=64, alias="lineLenght"
    ),
) -> dict:
    return {"status": "ok", "detail": f"Reciept {id} was send to {data.email}"}


@router.post("/{id}/send-to-messenger")
async def send_receipt_to_messenger(
    id: uuid.UUID,
    service: service_dependency,
    data: Messenger,
    line_lenght: int | None = Query(
        default=DEFAULT_LINE_LENGTH, ge=16, le=64, alias="lineLenght"
    ),
) -> dict:
    return {"status": "ok", "detail": f"Reciept {id} was send to {data.user}"}
