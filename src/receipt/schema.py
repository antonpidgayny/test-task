from datetime import datetime
from typing import Annotated
from pydantic import UUID4, Field, EmailStr

from ..schema import CustomBase, str_field_128
from .models import PaymentType

total_type = Annotated[
    float,
    Field(
        default=0,
        name="Сумарна ціна",
        description="Сумарна вартість усіх товарів у чеку",
    ),
]
rest_type = Annotated[
    float,
    Field(
        default=0,
        name="Решта",
        description="Решта з оплати. Складає: сума оплати - сумарна ціна",
    ),
]
payment_type = Annotated[
    PaymentType,
    Field(name="Тип оплати", description='Тип оплати: "cash" або "cashless"'),
]
amount_type = Annotated[float, Field(name="Сума оплати", description="Сума оплати")]
created_at_type = Annotated[
    datetime,
    Field(
        name="Дата-час створення",
        description='Дата-час створення чеку у форматі: "YYYY-MM-DDTHH:MM:SSZ"',
    ),
]
id_type = Annotated[UUID4, Field(name="ID чеку", description="ID чеку у форматі uuid4")]


class ProductPost(CustomBase):
    name: str_field_128 = Field(name="Назва", description="Назва продукту")
    price_per_unit: float = Field(
        name="Ціна", description="Ціна продукту за одиницю або кілограм"
    )
    quantity: float = Field(name="Кількість", description="Кількість одиниць або вага")


class PaymentPost(CustomBase):
    type: payment_type
    amount: amount_type


class RecieptPost(CustomBase):
    products: list[ProductPost]
    payment: PaymentPost


class ProductGet(ProductPost):
    total: total_type


class PaymentGet(PaymentPost): ...


class RecieptGet(RecieptPost):
    id: id_type
    products: list[ProductGet] = Field(
        name="Товари", description="Список товарів у чеку"
    )
    payment: PaymentGet = Field(name="Оплата", description="Оплата чеку")
    total: total_type
    rest: rest_type
    created_at: created_at_type


class RecieptGetAll(CustomBase):
    id: id_type
    total: total_type
    rest: rest_type
    payment_amount: amount_type
    payment_type: payment_type
    created_at: created_at_type


class Email(CustomBase):
    email: EmailStr


class Messenger(CustomBase):
    user: str
