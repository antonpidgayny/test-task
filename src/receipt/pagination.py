from pydantic import BaseModel, Field
from typing import Annotated
from fastapi import Depends
from sqlalchemy.orm import Query


class Paginator(BaseModel):
    page_size: int = Field(default=5, ge=5, le=50, alias="pageSize")
    page_number: int = Field(default=1, ge=1, alias="pageNumber")

    def paginate(self, qry: Query) -> Query:
        limit = self.page_size
        offset = (self.page_number - 1) * self.page_size
        return qry.limit(limit).offset(offset)


paginator_dependency = Annotated[Paginator, Depends()]
