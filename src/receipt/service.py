import uuid
from typing import Annotated
from fastapi import Depends
from ..auth.models import User
from .crud import crud_dependency, RecieptCrud
from .schema import (
    RecieptPost,
    RecieptGetAll,
    RecieptGet,
)
from .pagination import Paginator
from .filters import RecieptFilter
from .exceptions import RecieptNotFound
from .utils import RecieptFormater


class RecieptService:
    def __init__(self, crud: crud_dependency):
        self.crud: RecieptCrud = crud

    def get_all_receipts(
        self, filter: RecieptFilter, user: User, paginator: Paginator
    ) -> list[RecieptGetAll]:
        reciepts = self.crud.get_all_receipts(filter, user, paginator)
        res = [
            RecieptGetAll(
                payment_type=rec.payment.type,
                payment_amount=rec.payment.amount,
                **rec.__dict__,
            )
            for rec in reciepts
        ]
        return res

    def get_receipt_with_id(self, id: uuid.UUID, user: User) -> RecieptGet:
        reciept = self.crud.get_receipt_with_id(id=id, user=user)
        if reciept is None:
            raise RecieptNotFound(id)
        return RecieptGet.from_orm(reciept)

    def get_receipt_with_id_no_auth(self, id: uuid.UUID) -> RecieptGet:
        reciept = self.crud.get_receipt_with_id_no_auth(id=id)
        if reciept is None:
            raise RecieptNotFound(id)
        return RecieptGet.from_orm(reciept)

    def get_receipt_text(self, id: uuid.UUID, **kwargs) -> str:
        reciept = self.crud.get_receipt_with_id_no_auth(id=id)
        if reciept is None:
            raise RecieptNotFound(id)
        formater = RecieptFormater(receipt=reciept, **kwargs)
        return formater.get_reciept_text_visual()

    def create_receipt(self, data: RecieptPost, user: User) -> RecieptGet:
        reciept = self.crud.create_receipt(data, user)
        return RecieptGet.from_orm(reciept)


service_dependency = Annotated[RecieptService, Depends(RecieptService)]
