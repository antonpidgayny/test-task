from pydantic import (
    BaseModel,
    Field,
    ConfigDict,
)
from typing import Annotated


str_field_128 = Annotated[str, Field(max_length=128, min_length=1)]


class CustomBase(BaseModel):
    model_config = ConfigDict(from_attributes=True)
