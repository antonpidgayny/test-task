from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, declarative_base

from .config import (
    DB_URL,
)


engine = create_engine(DB_URL, echo=False)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


def init_db():
    Base.metadata.create_all(engine)
