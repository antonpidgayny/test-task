import os

DB_URL = os.environ.get("DB_URL")
DOMAIN = os.environ.get("DOMAIN", "http://127.0.0.1:8000")
