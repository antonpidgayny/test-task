from fastapi import FastAPI

from .db import (
    init_db,
)
from .receipt.router import router as receipt_router
from .auth.router import router as auth_router


def lifespan(app: FastAPI):
    init_db()
    yield


app = FastAPI(title="TestTaskAPI", lifespan=lifespan)


app.include_router(receipt_router)
app.include_router(auth_router)
