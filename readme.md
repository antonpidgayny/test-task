# Тестове завдання - Python Backend Developer

REST API для створення та перегляду чеків з реєстрацією та авторизацією користувачів.


## Requirements
- docker compose
## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/antonpidgayny/test-task.git
```

Go to the project directory

```bash
  cd test-task
```

Start the server

```bash
  bash scripts/run-dev.sh -d --build 
```


## Running Tests

To run tests, run the following command

```bash
  bash scripts/run-test.sh
```

## Environment Variables

You may add the following environment variables to your .env file

`POSTGRES_USER` (default value: api_user)

`POSTGRES_PASSWORD` (default value: api_pass)

`POSTGRES_DB` (default value: api)

`PASS_HASH_SALT` (default value: default_hash_salt)

`SECRET_KEY` (default value: default_secret_key)


## Documentation

After starting the server documentation can be accessed with the following links:

[Docs](http://127.0.0.1:8000/docs)

[Redoc](http://127.0.0.1:8000/redoc)


## Demo

[Hosted demo](https://anton-dev.pp.ua/docs)